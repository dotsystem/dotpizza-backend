echo "
CREATE TABLE Client(
    id INT,
    cpf VARCHAR(15),
    name VARCHAR(100) NOT NULL,
    phone VARCHAR(11) NOT NULL,
    cellPhone VARCHAR(11),
    sex VARCHAR(10),
    birth DATE,
    observation VARCHAR(255),
    status BOOLEAN,
    email VARCHAR(100),
    password VARCHAR(20),

    CONSTRAINT pk_client PRIMARY KEY (id)
);

CREATE TABLE Address(
    id INT,
    description VARCHAR(100) NOT NULL,
    zipCode VARCHAR(8) NOT NULL,
    street VARCHAR(100) NOT NULL,
    number INT NOT NULL,
    complement VARCHAR(50),
    neighborhood VARCHAR(20) NOT NULL,
    city VARCHAR(50) NOT NULL,
    state VARCHAR(25) NOT NULL,
    country VARCHAR(25),
    status BOOLEAN,
    clientId INT,

    CONSTRAINT pk_address PRIMARY KEY (id),
    CONSTRAINT fk_add_client FOREIGN KEY (clientId) REFERENCES Client (id)
);

CREATE TABLE Employer(
    id INT,
    cpf VARCHAR(15) NOT NULL,
    rg VARCHAR(15) NOT NULL,
    name VARCHAR(100) NOT NULL,
    phone VARCHAR(11) NOT NULL,
    cellPhone VARCHAR(11),
    sex VARCHAR(10),
    birth DATE,
    observation VARCHAR(255),
    status BOOLEAN,
    email VARCHAR(100),
    password VARCHAR(20),
    addressId INT,

    CONSTRAINT pk_employer PRIMARY KEY (id),
    CONSTRAINT fk_employer_address FOREIGN KEY (addressId) REFERENCES Address (id)
);

CREATE TABLE Store(
    id INT,
    companyName VARCHAR(50) NOT NULL,
    cnpj VARCHAR(20) NOT NULL,
    phone VARCHAR(11) NOT NULL,
    status BOOLEAN,
    addressId INT,

    CONSTRAINT pk_store PRIMARY KEY (id),
    CONSTRAINT fk_store_address FOREIGN KEY (addressId) REFERENCES Address (id)
);

CREATE TABLE Tax(
    id INT,
    description VARCHAR(20) NOT NULL,
    value NUMERIC(4,2),
    observation VARCHAR(255),

    CONSTRAINT pk_tax PRIMARY KEY (id)
);

CREATE TABLE Request(
    id INT,
    dateHour DATE,
    clientId INT,
    storeId INT,
    tax INT,
    total NUMERIC(16,2),
    status BOOLEAN,

    CONSTRAINT pk_request PRIMARY KEY (id),
    CONSTRAINT fk_request_client FOREIGN KEY (clientId) REFERENCES Client (id),
    CONSTRAINT fk_request_store FOREIGN KEY (storeId) REFERENCES Store (id),
    CONSTRAINT fk_request_tax FOREIGN KEY (tax) REFERENCES Tax (id)
);

CREATE TABLE Category(
    id INT,
    description VARCHAR(50) NOT NULL,
    status BOOLEAN,

    CONSTRAINT pk_category PRIMARY KEY (id)
);

CREATE TABLE Product(
    id INT,
    title VARCHAR(25) NOT NULL,
    description VARCHAR(255),
    unit VARCHAR(50),
    salePrice NUMERIC(16,2) NOT NULL,
    type VARCHAR(25),
    status BOOLEAN NOT NULL,
    categoryId INT,

    CONSTRAINT pk_product PRIMARY KEY (id)
);

CREATE TABLE RequestItem(
    requestId INT,
    productId INT,
    quantity INT NOT NULL,
    subtotal NUMERIC(16,2),

    CONSTRAINT pk_requestItem PRIMARY KEY (requestId, productId),
    CONSTRAINT fk_requestItem_order FOREIGN KEY (requestId) REFERENCES Request (id),
    CONSTRAINT fk_requestItem_product FOREIGN KEY (productId) REFERENCES Product (id)
);

CREATE TABLE Ingredients(
    id INT,
    description VARCHAR(50) NOT NULL,

    CONSTRAINT pk_ingredients PRIMARY KEY (id)
);

CREATE TABLE ProductItem(
    productId INT,
    ingredientsId INT,
    quantity INT,
    status BOOLEAN,

    CONSTRAINT pk_productItem PRIMARY KEY (productId, ingredientsId),
    CONSTRAINT fk_productItem_prod FOREIGN KEY (productId) REFERENCES Product (id),
    CONSTRAINT fk_productItem_ingred FOREIGN KEY (ingredientsId) REFERENCES Ingredients (id)
);

CREATE TABLE Payment(
    id INT,
    description VARCHAR(50) NOT NULL,
    status BOOLEAN NOT NULL,

    CONSTRAINT pk_payment PRIMARY KEY (id)
);

CREATE TABLE PaymentItem(
    paymentId INT,
    requestId INT,
    value NUMERIC(16,2),

    CONSTRAINT pk_paymentItem PRIMARY KEY (paymentId, requestId),
    CONSTRAINT fk_paymentItem_payment FOREIGN KEY (paymentId) REFERENCES Payment (id),
    CONSTRAINT fk_paymentItem_order FOREIGN KEY (requestId) REFERENCES Request (id)
);

CREATE TABLE Promotion(
    id INT,
    status BOOLEAN,
    startDate DATE,
    stopDate DATE,
    sunday BOOLEAN,
    monday BOOLEAN,
    tuesday BOOLEAN,
    wednesday BOOLEAN,
    thursday BOOLEAN,
    friday BOOLEAN,
    saturday BOOLEAN,
    price NUMERIC(16,2),

    CONSTRAINT pk_promotion PRIMARY KEY (id)
);

CREATE TABLE PromotionItem(
    productId INT,
    promotionId INT,

    CONSTRAINT pk_promotionItem PRIMARY KEY (productId, promotionId),
    CONSTRAINT fk_promotionItem_product FOREIGN KEY (productId) REFERENCES Product (id),
    CONSTRAINT fk_promotionItem_promotion FOREIGN KEY (promotionId) REFERENCES Promotion (id)
);
"