package integration

import spock.lang.Specification

class BootstrapTest extends Specification {

    def "Bootstrap is starting embedded jetty"() {
        when:
            def result = "http://127.0.0.1:4567".toURL().text
        then:
            result == "Hello World from DotPizza ;)"
    }

}
