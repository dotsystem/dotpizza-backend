package unit

import br.com.dotpizza.api.Math

class MathTest extends spock.lang.Specification {

    def "Math must sum correctly"() {
        given:
            def math = new Math()
        when:
            def resoult = math.sum(2.2,2.2)
        then:
            resoult == 4.4
    }
}
